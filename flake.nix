{
  description = "reveal presentation";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
      this-py = pkgs.python311;
      py-deps = ps: with ps; [
        # Python packages
        livereload
      ];
      deps = with pkgs; [
        (this-py.withPackages py-deps)

        # system packages
        #bash
      ];
    in {
      devShells.default = pkgs.mkShell rec {
        packages = deps;
      };
    });
}
